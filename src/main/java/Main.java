public class Main {
    public static void main(String[] args) {
        Bmw bmw=new Bmw();
        Class bmw1=bmw.getClass();
        Car bmw2= (Car) bmw1.getAnnotation(Car.class);
        System.out.println(bmw2.hp());

        VW vw=new VW();
        Class vw1=vw.getClass();
        Car vw2= (Car) vw1.getAnnotation(Car.class);
        System.out.println(vw2.hp());
    }
}
